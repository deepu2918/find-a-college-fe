import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SearchCourseComponent } from './search-course/search-course.component';

const routes: Routes = [
  { path: '', component: SearchCourseComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {
      scrollPositionRestoration: 'enabled', // Add options right here
    })
  ],
  declarations: [],
  exports: [RouterModule],
})
export class AppRoutingModule { }
