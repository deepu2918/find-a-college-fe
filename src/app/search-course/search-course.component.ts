import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RestServiceService } from './../shared/service/rest-service.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HttpParams } from '@angular/common/http';
import { useAnimation } from '@angular/animations';

export interface Courses {
  id?: number;
  name: string;
  description: string;
  country: string;
  universityName: string;
  teacherName: string;
  gpa: number;
  greScore: number;
}

@Component({
  selector: 'app-search-course',
  templateUrl: './search-course.component.html',
  styleUrls: ['./search-course.component.css']
})
export class SearchCourseComponent implements OnInit {

  searchCourseForm: FormGroup;
  courses: Courses[];
  countries;
  loading = false;
  submitted = false;

  constructor(private http: RestServiceService,
    private router: Router,
    private formBuilder: FormBuilder) { }

  ngOnInit() {
    this.searchCourseForm = this.formBuilder.group({
      gpa: ['', Validators.required],
      gre: ['', Validators.required],
      country: ['', Validators.required],
      course: ['', Validators.nullValidator],
    });
    this.countries = [{ code: 'USA', name: 'United States' }, { code: 'UK', name: 'United Kingdom' }, { code: 'AU', name: 'Australia' }];
  }

  get f() { return this.searchCourseForm.controls; }

  searchCourse() {
    this.submitted = true;
    console.log(this.searchCourseForm.invalid);
    if (this.searchCourseForm.invalid) {
      return;
    }
    const payload = this.searchCourseForm.value;
    console.log('searchCourseFormmmm:::', payload);
    const params = new HttpParams();
    for (const key of Object.keys(payload)) {
      console.log(key); // prints values: 10, 20, 30, 40
      params.append(key, payload[key]);
    }
    console.log('paramssss:::', params);
    this.http.getWithParams('search-course', payload).subscribe(data => {
      this.courses = data['data'].courses;
    });
  }

}
