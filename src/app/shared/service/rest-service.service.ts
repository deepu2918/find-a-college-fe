import { HttpClient, HttpHeaders, HttpParams, HttpRequest, } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from './../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestServiceService {
  apiUrl: string = environment.apiUrl;
  constructor(private router: Router, private httpClient: HttpClient,
  ) { }

  getWithParams(url, params) {
    return this.httpClient.get(`${this.apiUrl}${url}`, { params });
  }

  getDirect(url) {
    return this.httpClient.get(url);
  }

}
