## Install dependencies
```
npm i
```

## Start Application
```
npm start
```
Application will start on 4200 port navigate to below url

```
http://localhost:4200/
```